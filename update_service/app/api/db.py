import os
from pydantic import BaseModel
from sqlalchemy import create_engine


DATABASE_URI = os.getenv("DATABASE_URI")

engine = create_engine(DATABASE_URI)


class MahasiswaIn(BaseModel):
    npm: str
    nama: str
