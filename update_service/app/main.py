from fastapi import FastAPI
from app.api.db import MahasiswaIn
from app.api.update_mahasiswa import update_mahasiswa

app = FastAPI()


@app.post("/update", status_code=200)
async def update_mhs(mahasiswa: MahasiswaIn):
    await update_mahasiswa(mahasiswa)
    return {"status": "OK"}
