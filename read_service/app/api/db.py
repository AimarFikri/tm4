import os

from sqlalchemy import (Column, String, MetaData, Table, create_engine)
from databases import Database

# Database Creation
DATABASE_URI = os.getenv("DATABASE_URI")

engine = create_engine(DATABASE_URI)
meta = MetaData()

mahasiswa = Table(
    "mahasiswa", meta,
    Column("npm", String, primary_key=True),
    Column("nama", String)
)

database = Database(DATABASE_URI)
