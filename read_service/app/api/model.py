from pydantic import BaseModel


class StatusOut(BaseModel):
    status: str


class MahasiswaIn(BaseModel):
    npm: str
    nama: str


class MahasiswaOut(MahasiswaIn, StatusOut):
    pass
