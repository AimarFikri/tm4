from app.api.db import database, mahasiswa
from app.api.model import MahasiswaIn


async def add_mahasiswa(payload: MahasiswaIn):
    query = mahasiswa.insert().values(**payload.dict())
    return await database.execute(query=query)


async def get_mahasiswa(npm: str):
    query = mahasiswa.select(mahasiswa.c.npm == npm)
    return await database.fetch_one(query=query)
