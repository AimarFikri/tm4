from fastapi import APIRouter, HTTPException

from app.api.model import MahasiswaIn, MahasiswaOut
from app.api.db_manager import add_mahasiswa, get_mahasiswa

read_mahasiswa = APIRouter()

@read_mahasiswa.get("/ping")
async def ping():
    return {"message": "pong"}

@read_mahasiswa.post("/", response_model=MahasiswaIn, status_code=201)
async def create_mahasiswa(payload: MahasiswaIn):
    await add_mahasiswa(payload)
    return payload.dict()

@read_mahasiswa.get("/{npm}", response_model=MahasiswaOut, status_code=200)
async def retrieve_mahasiswa(npm: str):
    data = await get_mahasiswa(npm)
    if not data:
        raise HTTPException(status_code=404, detail="Mahasiswa with npm not found")
    return {
        "status":"OK",
        **data
    }
