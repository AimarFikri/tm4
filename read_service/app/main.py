from fastapi import FastAPI
from app.api.db import meta, database, engine
from app.api.read_mahasiswa import read_mahasiswa

meta.create_all(engine)

app = FastAPI()

@app.on_event("startup")
async def startup():
    await database.connect()

@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()

app.include_router(read_mahasiswa, prefix='/read', tags=['read'])
